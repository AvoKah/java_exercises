package MorseConverter;

import java.util.*;

/**
 * Created by kev on 26/11/16.
 */
public class Converter {

    private static HashMap<Character, String> strToMorse;
    private static HashMap<String, Character> morseToStr;

    public Converter()
    {
        strToMorse = new HashMap<>();
        strToMorse.put('A', ".-");
        strToMorse.put('B', "-...");
        strToMorse.put('C', "-.-.");
        strToMorse.put('D', "-..");
        strToMorse.put('E', ".");
        strToMorse.put('F', "..-.");
        strToMorse.put('G', "--.");
        strToMorse.put('H', "....");
        strToMorse.put('I', "..");
        strToMorse.put('J', ".---");
        strToMorse.put('K', "-.-");
        strToMorse.put('L', ".-..");
        strToMorse.put('M', "--");
        strToMorse.put('N', "-.");
        strToMorse.put('O', "---");
        strToMorse.put('P', ".--.");
        strToMorse.put('Q', "--.-");
        strToMorse.put('R', "-.-");
        strToMorse.put('S', "...");
        strToMorse.put('T', "-");
        strToMorse.put('U', "..-");
        strToMorse.put('V', "...-");
        strToMorse.put('W', ".--");
        strToMorse.put('X', "-..-");
        strToMorse.put('Y', "-.--");
        strToMorse.put('Z', "--..");

        strToMorse.put('1', ".----");
        strToMorse.put('2', "..---");
        strToMorse.put('3', "...--");
        strToMorse.put('4', "....-");
        strToMorse.put('5', ".....");
        strToMorse.put('6', "-....");
        strToMorse.put('7', "--...");
        strToMorse.put('8', "---..");
        strToMorse.put('9', "----.");
        strToMorse.put('0', "-----");

        strToMorse.put('a', ",_");
        strToMorse.put('b', "_,,,");
        strToMorse.put('c', "_,_,");
        strToMorse.put('d', "_,,");
        strToMorse.put('e', ",");
        strToMorse.put('f', ",,_,");
        strToMorse.put('g', "__,");
        strToMorse.put('h', ",,,,");
        strToMorse.put('i', ",,");
        strToMorse.put('j', ",___");
        strToMorse.put('k', "_,_");
        strToMorse.put('l', ",_,,");
        strToMorse.put('m', "__");
        strToMorse.put('n', "_,");
        strToMorse.put('o', "___");
        strToMorse.put('p', ",__,");
        strToMorse.put('q', "__,_");
        strToMorse.put('r', "_,_");
        strToMorse.put('s', ",,,");
        strToMorse.put('t', "_");
        strToMorse.put('u', ",,_");
        strToMorse.put('v', ",,,_");
        strToMorse.put('w', ",__");
        strToMorse.put('x', "_,,_");
        strToMorse.put('y', "_,__");
        strToMorse.put('z', "__,,");

        strToMorse.put('1', ".----");
        strToMorse.put('2', "..---");
        strToMorse.put('3', "...--");
        strToMorse.put('4', "....-");
        strToMorse.put('5', ".....");
        strToMorse.put('6', "-....");
        strToMorse.put('7', "--...");
        strToMorse.put('8', "---..");
        strToMorse.put('9', "----.");
        strToMorse.put('0', "-----");

        morseToStr = new HashMap<>();
        for (Map.Entry<Character, String> entry : strToMorse.entrySet())
            morseToStr.put(entry.getValue(), entry.getKey());
    }

    private static void toMorse(String[] args)
    {
        String res = new String();
        for (String arg : args)
        {
            for (char c : arg.toCharArray())
            {
                if (strToMorse.containsKey(c))
                    res += strToMorse.get(c);
                else
                {
                    if (c == '.')
                        res += '\\';
                    res += c;
                }
                res += "  ";
            }
            res += '\n';
        }
        System.out.println(res);
    }

    private static void fromMorse(String[] args)
    {
        String res = new String();
        for (String arg : args)
        {
            for (String morseLetter : arg.split(" ")) {
                if (morseToStr.containsKey(morseLetter))
                    res += morseToStr.get(morseLetter);
                else
                {
                    if (morseLetter.length() > 1 && morseLetter.charAt(0) == '\\')
                        res += morseLetter.charAt(1);
                    else
                        res += morseLetter;
                }
            }
            res += ' ';
        }
        System.out.println(res);
    }

    public static void run(String[] args)
    {
        Converter conv = new Converter();
        char c = args[0].charAt(0);
        if (Character.isLetter(c) || Character.isDigit(c))
            toMorse(args);
        else if (c == '.' || c == '-' || c == ',' || c == '_')
            fromMorse(args);
        else
            System.out.println("Please, just type a correct entry :(");
    }
}
