import MorseConverter.Converter;

/**
 * Created by kev on 26/11/16.
 */
public class Main
{
    public static void main(String[] args)
    {
        if (args.length < 1) {
            System.out.println("Error : \033[91mMISSING ARGUMENTS\033[0m. This is a morse converter. You then need a string to convert");
            System.exit(1);
            return;
        }
        Converter.run(args);
    }
}
